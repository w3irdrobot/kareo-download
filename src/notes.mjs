//@ts-check
//@ts-expect-error
import { KAREO_BASE_URL } from './helpers.mjs';
import { setTimeout } from 'timers/promises';
import { mkdtemp, rm } from 'fs/promises';
import path from 'path';
import os from 'os';
import playwright from 'playwright';

const KAREO_PATIENT_PAGE = `${KAREO_BASE_URL}/EhrWebApp/patients/viewFacesheet`;
const KAREO_NOTES_NAV_IFRAME = '//*[@id="patient-navigation"]/div/iframe';
const KAREO_NOTES_NAV = '//*[@id="facesheet-root"]/div/ul/li[2]/a[8]';
const TREATMENT_PLANS_TABLE = 'div[data-testid="treatmentPlanList"]';
const TREATMENT_PLANS_TABLE_ROWS = `${TREATMENT_PLANS_TABLE} > .list-item`;
const NOTES_TABLE = '.primary-column > .card.list';
const NOTES_NURSE_IMAGE = '.primary-column .mbs > .grid-4.offset-left-4 > img';
const NOTES_TABLE_ROWS = `${NOTES_TABLE} > .list-item`;
const NOTES_TABLE_PAGER = `${NOTES_TABLE} ul.pagination`;
const NOTES_TABLE_PAGE_NUMBER = `${NOTES_TABLE_PAGER} > li:nth-last-child(2) > a`;
const NOTES_TABLE_NEXT = `${NOTES_TABLE_PAGER} > li:last-child > a`;
const TYPE_FROM_ITEM = `.grid-9 > div:first-child h3`;
const DOS_FROM_ITEM = `.grid-9 > div:last-child > div:last-child > span > strong`;

/**
 *
 * @param {playwright.Page} page
 * @param {string} patientId
 */
export const navigate = async (page, patientId) => {
  await page.goto(`${KAREO_PATIENT_PAGE}/${patientId}`, {
    waitUntil: 'domcontentloaded',
  });

  // get notes nav inside notes iframe
  const navButton = await page
    .$(KAREO_NOTES_NAV_IFRAME)
    .then((iframe) => iframe.contentFrame())
    .then((frame) => frame.waitForSelector(KAREO_NOTES_NAV));

  await Promise.all([
    page.waitForNavigation({ waitUntil: 'load' }),
    navButton.click(),
  ]);
};

/**
 *
 * @param {playwright.Page} page
 */
export const numberOfPages = async (page) => {
  // timeouts are staggered to ensure the next button timeout is the default
  const displayed = await Promise.race([
    // wait for next button. hopefully the last page number exists before the next button does ¯\_(ツ)_/¯
    page
      .waitForSelector(NOTES_TABLE_NEXT, { timeout: 4000 })
      // this is horrible. i know.
      .then(() => NOTES_TABLE_NEXT)
      .catch((e) => 'fail'),
    // also waiting for nurse picture thing to pop up
    page
      .waitForSelector(NOTES_NURSE_IMAGE, { timeout: 5000 })
      .then(() => NOTES_NURSE_IMAGE),
  ]);

  if (displayed === NOTES_NURSE_IMAGE) {
    // if nurse picture, return zero since no notes exist
    return 0;
  } else if (displayed === 'fail') {
    // nurse didn't show up and next timed out. this means
    // there is just a single page
    return 1;
  }

  const n = await page.$eval(NOTES_TABLE_PAGE_NUMBER, (el) =>
    el.textContent.trim()
  );
  return parseInt(n, 10);
};

/**
 * @param {playwright.Page} page
 * @param {number} pageNumber
 */
const paginateToNotesPage = async (page, pageNumber) => {
  for (let i = 0; i < pageNumber - 1; i++) {
    await nextPageOfNotes(page);
  }
};

/**
 *
 * @param {playwright.Page} page
 */
export const nextPageOfNotes = async (page) => {
  try {
    const nextButton = await getNextButtonWithRefresh(page);
    await Promise.all([waitForLoader(), nextButton.click()]);
  } catch (e) {
    console.error('error attempting to go to the next page of notes');
    console.error('url:', page.url());
    throw e;
  }
};

/**
 *
 * @param {playwright.Page} page
 */
const getNextButtonWithRefresh = async (page) => {
  try {
    return await page.waitForSelector(NOTES_TABLE_NEXT);
  } catch (e) {
    // try refreshing the page. it's been noticed that sometimes
    // the page navigates but the UI is empty. a reload seems to
    // "fix the glitch"
    await page.reload();
  }
  // only try reloading once. if that doesn't fix it, then
  // the issue could be a real issue. therefore, we should
  // fallback to whatever the caller wants to use to handle
  // the error
  return await page.waitForSelector(NOTES_TABLE_NEXT);
};

/**
 *
 * @param {playwright.Page} page
 * @param {number} pageNumber
 */
const getNotesRowsForPage = async (page, pageNumber) => {
  await paginateToNotesPage(page, pageNumber);
  return await page.$$(NOTES_TABLE_ROWS);
};

/**
 *
 * @param {playwright.Page} page
 */
const getTreatmentRowsForPage = async (page) =>
  await page.$$(TREATMENT_PLANS_TABLE_ROWS);

/**
 *
 * @param {playwright.Page} page
 */
const goBackAndGetTreatmentRowsForPage = async (page) => {
  await page.goBack();
  await setTimeout(2000);
  return await getTreatmentRowsForPage(page);
};

const waitForLoader = async () => await setTimeout(1000);

/**
 *
 * @param {playwright.Page} page
 * @param {number} pageNumber
 */
const goBackAndGetNoteRowsForPage = async (page, pageNumber) => {
  await page.goBack();
  await setTimeout(2000);
  return await getNotesRowsForPage(page, pageNumber);
};

/**
 *
 * @param {playwright.Page} page
 * @param {number} pageNumber
 * @param {Array} cache
 */
export async function* getCurrentTableRows(name, page, pageNumber, cache) {
  let treatmentRows = await getTreatmentRowsForPage(page);
  const treatmentTempDir = await mkdtemp(path.join(os.tmpdir(), 'treatment-'));
  for (let i = 0; i < treatmentRows.length; i++) {
    const row = treatmentRows[i];
    const date = (
      await row.$eval(DOS_FROM_ITEM, (el) => el.textContent.trim())
    ).split(' ')[0];

    if (date === '(last') {
      // handle when no start date available
      console.log(`no start date on treatment for ${name}. skipping...`);
      continue;
    }
    const label = await row.$eval(TYPE_FROM_ITEM, (el) =>
      el.textContent.trim()
    );
    const openButton = await row.$(
      '.list-content > .grid-3 .small.right.button'
    );
    await Promise.all([page.waitForNavigation(), openButton.click()]);

    const splitUrl = page.url().split('/');
    const noteId = splitUrl[splitUrl.length - 1];
    const cache_id = `${date}_${label}_${noteId}`;

    if (cache.includes(cache_id)) {
      treatmentRows = await goBackAndGetTreatmentRowsForPage(page);
      continue;
    }

    const printButton = await page.waitForSelector(
      'button[data-testid="printTreatmentPlan"]'
    );
    const [popup] = await Promise.all([
      page.waitForEvent('popup'),
      printButton.click(),
    ]);
    const tempFilePath = path.join(treatmentTempDir, `${noteId}.pdf`);
    await popup.pdf({ path: tempFilePath });

    cache.push(cache_id);
    yield { date, label, noteId, path: tempFilePath };

    await popup.close();
    treatmentRows = await goBackAndGetTreatmentRowsForPage(page);
  }
  await rm(treatmentTempDir, { recursive: true });

  let noteRows = await getNotesRowsForPage(page, pageNumber);
  for (let i = 0; i < noteRows.length; i++) {
    const row = noteRows[i];
    const signedSpan = await row.$(
      'span.ng-scope > span.ng-scope > span:nth-child(2)'
    );

    if (await signedSpan.isHidden()) {
      // note isn't signed. skip this
      continue;
    }

    const date = (
      await row.$eval(DOS_FROM_ITEM, (el) => el.textContent.trim())
    ).split(' ')[0];
    const label = await row.$eval(TYPE_FROM_ITEM, (el) =>
      el.textContent.trim()
    );

    // go to note page for downloading
    const viewButton = await row.$(
      '.list-content > .grid-3 .small.right.button'
    );
    await Promise.all([page.waitForNavigation(), viewButton.click()]);

    const splitUrl = page.url().split('/');
    const noteId = splitUrl[splitUrl.length - 1];
    const cache_id = `${date}_${label}_${noteId}`;

    if (cache.includes(cache_id)) {
      noteRows = await goBackAndGetNoteRowsForPage(page, pageNumber);
      continue;
    }

    try {
      const printButton = await page.$('#NotesPrintController');
      const [popup, download] = await Promise.all([
        page.waitForEvent('popup'),
        page.waitForEvent('download'),
        printButton.click(),
      ]);
      const path = await download.path();

      cache.push(cache_id);
      yield { date, label, noteId, path };

      await popup.close();
    } catch (e) {
      console.error(`error downloading a file from page "${page.url()}"`);
    }
    // get rows again because it appears navigating between pages
    // messes up the internal context of rows
    noteRows = await goBackAndGetNoteRowsForPage(page, pageNumber);
  }
}
